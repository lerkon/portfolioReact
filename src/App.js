import React from 'react';
import Page from './Components/Page';

const App = React.memo(() => {
  return <Page />;
});

export default App;