import React, { cloneElement, useEffect, useRef, useState } from "react";
import { Popper, Grow, Paper, ClickAwayListener, MenuList } from '@material-ui/core';

const HoverDropdown = React.memo(({ hoverable, children, ...props }) => {
  const [open, setOpen] = useState(false);
  const componentRef = useRef();
  const timeoutId = useRef();

  useEffect(() => {
    return () => clearTimeout(timeoutId.current);
  }, []);

  const handleClose = () => {
    clearTimeout(timeoutId.current);
    setOpen(false);
  };

  const handleDelayedClose = () => {
    clearTimeout(timeoutId.current);
    timeoutId.current = setTimeout(() => setOpen(false), 400);
  };

  const handleOpen = () => {
    clearTimeout(timeoutId.current);
    setOpen(true);
  };

  const menu = children.map((e) => {
    const onClick = () => {
      handleClose();
      e.props.onClick?.();
    }
    return cloneElement(e, { onClick });
  })

  return (
    <>
      {cloneElement(hoverable, {
        ref: componentRef,
        'aria-controls': open ? 'menu-list-grow' : undefined,
        'aria-haspopup': true,
        onClick: handleOpen,
        onMouseEnter: handleOpen,
        onMouseLeave: handleDelayedClose
      })}
      <Popper
        open={open}
        anchorEl={componentRef.current}
        role={undefined}
        onMouseEnter={handleOpen}
        onMouseLeave={handleDelayedClose}
        transition
        disablePortal
        style={{ zIndex: '1' }}
        {...props}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList autoFocusItem={open} id="menu-list-grow">
                  {menu}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
});

export default HoverDropdown;