import React, { createContext, useContext, useEffect, useRef, useState } from 'react';
import { Grid, Button, TextField } from '@material-ui/core';

const FormContext = createContext();
FormContext.displayName = 'FormContext';

const REG_EXPRS = {
  email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
}

const TYPES = {
  textField: TextField,
  button: Button
}

export const FormItem = React.memo(({
  component, grid, fullWidth=true, error: initError, required, name, validator, ...props
}) => {
  const [error, setError] = useState();
  const formRef = useContext(FormContext);
  const inputRef = useRef();

  useEffect(() => setError(initError), [initError]);

  const isValid = () => {
    const str = inputRef.current.value;
    let isValid = true;
    if (required === true) {
      isValid = !!str.trim();
    }
    if (isValid && validator?.constructor === RegExp) {
      isValid = validator.test(str);
    } else if (isValid && REG_EXPRS[validator]) {
      isValid = REG_EXPRS[validator].test(str);
    }
    setError(!isValid);
    return isValid;
  }

  if (name) {
    props.inputRef = inputRef;
    props.onBlur= isValid;
    if (!formRef.current[name]) {
      formRef.current[name] = {};
    }
    formRef.current[name].isValid = isValid;
    formRef.current[name].inputRef = inputRef;
  }
  
  const Component = TYPES[component] || TYPES.textField;

  return (
    <Grid {...grid} item>
      <Component
        {...props}
        fullWidth={fullWidth}
        error={error}
        required={required}
        name={name}
      />
    </Grid>
  )
});

export const Form = React.memo(({
  action, method='POST', onSubmit, spacing=2, children, grid, raw=false, ...props
}) => {
  const formRef = useRef({});

  const onSubmitClb = (event) => {
    let ok = true;
    const values = {};
    for (const name in formRef.current) {
      const obj = formRef.current[name];
      const isValid = obj.isValid();
      if (!isValid) {
        ok = false;
      }
      values[name] = obj.inputRef.current.value;
    }
    const response = [event, { ok, values }];
    if (!action || !ok) {
      event.preventDefault();
      onSubmit?.(...response);
      return;
    }
    if(raw) {
      return;
    }
    event.preventDefault();
    const data = new URLSearchParams(new FormData(event.target));
    const promise = fetch(action, { method: method, body: data });
    onSubmit?.(...response, promise);
  }

  if (raw) {
    props.action = action;
    props.method = method;
  }

  return (
    <FormContext.Provider value={formRef}>
      <form noValidate onSubmit={onSubmitClb} {...props}>
        <Grid container spacing={spacing} {...grid}>
          {children}
        </Grid>
      </form>
    </FormContext.Provider>
  );
});

export default Form;