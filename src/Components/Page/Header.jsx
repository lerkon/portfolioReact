import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import useTranslation from './../../hooks/useTranslation';
import HoverDropdown from './../HoverDropdown';
import {
  AppBar, IconButton, Toolbar, Typography, Button, Grid, Hidden, MenuItem, Drawer,
  Accordion, AccordionSummary, AccordionDetails
} from '@material-ui/core';
import { PermIdentity, Menu, Language, ExpandMore } from '@material-ui/icons';

const LANGUAGES = ['en', 'pl'];
const MENU_ITEMS = ['projects', 'contact'];

const useStyles = makeStyles(() => ({
  appBar: {
    margin: '25px 0px',
    backgroundColor: 'inherit',
    '& a': {
      textDecoration: 'inherit',
      color: 'inherit'
    },
    '& > *': {
      padding: '0px'
    },
    '& .title a': {
      display: 'flex',
      alignItems: 'center'
    },
    '& button:not(.MuiButton-outlined):not(.MuiIconButton-root)': {
      minWidth: '36px',
      '&:before': {
        content: '""',
        position: 'absolute',
        top: '100%',
        height: '0px',
        backgroundColor: '#000',
        left: '50%',
        right: '50%',
        transition: 'all 0.5s cubic-bezier(0.25, 1, 0.33, 1)'
      },
      '&:hover:before, &.active:before': {
        right: '0',
        left: '0',
        height: '1px'
      }
    }
  },
  drawer: {
    '& .menu-item': {
      width: '220px',
      margin: '0px',
      boxShadow: 'none',
      '&:before': {
        display: 'none'
      },
      '& .label': {
        minHeight: '0px',
        padding: '0px',
        '& > *': {
          margin: '0px',
        },
        '& .MuiAccordionSummary-expandIcon': {
          position: 'absolute',
          right: '0px'
        },
        '& a': {
          width: '100%',
          textDecoration: 'inherit'
        },
        '& button': {
          padding: '9px 0px 9px 0px',
          width: '100%',
          '& .MuiButton-label': {
            width: 'initial',
            padding: '6px 8px',
          },
          '&.active .MuiButton-label': {
            borderBottom: '1px solid black'
          },
          '& :hover': {
            backgroundColor: 'transparent'
          }
        },
      }
    },
    '& .sub-menu': {
      display: 'block',
      padding: '0px',
      backgroundColor: '#f5f5f5',
      '& button': {
        width: '100%',
        padding: '15px 0px',
        '&.active .MuiButton-label': {
          fontWeight: 'bold' 
        }
      }
    }
  }
}));

const Header = React.memo(() => {
  const { t, i18n } = useTranslation();
  const [isDrawer, setIsDrawer] = useState(false);
  const location = useLocation();
  const styles = useStyles();

  const HoverBtn = (
    <IconButton color="inherit" aria-label="menu">
      <Language />
    </IconButton>
  )
  
  return (
    <AppBar position="static" color="transparent" className={styles.appBar} elevation={0}>
      <Toolbar>
        <Typography variant="h6" className="title">
          <Link to="/">
            <PermIdentity/>&nbsp;{t('appLogoTxt')()}
          </Link>
        </Typography>
        <Grid
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          spacing={3}
        >
          <Hidden xsDown>
            {MENU_ITEMS.map((e) => {
              const className = location.pathname.startsWith(`/${e}`) ? 'active' : '';
              return (
                <Grid key={e} item>
                  <Link to={`/${e}`}>
                    <Button color="inherit" className={className}>
                      {t(e)()}
                    </Button>
                  </Link>
                </Grid>
              )
            })}
            <Grid item>
              <HoverDropdown hoverable={HoverBtn}>
                {LANGUAGES.map((e) => (
                  <MenuItem
                    key={e}
                    selected={e === i18n.language.split('-')[0]}
                    onClick={() => i18n.changeLanguage(e)}
                  >
                    {t(e)()}
                  </MenuItem>
                ))}
              </HoverDropdown>
            </Grid>
            {/* <Grid item>
              <Button color="primary" variant="outlined">
                <Link to="/contact">{t('contact')()}</Link>
              </Button>
            </Grid> */}
          </Hidden>
          <Hidden smUp>
            <IconButton color="inherit" aria-label="menu" onClick={() => setIsDrawer(true)}>
              <Menu />
            </IconButton>
          </Hidden>
        </Grid>
      </Toolbar>
      <Drawer
        anchor="left"
        open={isDrawer}
        onClose={() => setIsDrawer(false)}
        className={styles.drawer}
      >
        {MENU_ITEMS.map((e) => {
          const className = location.pathname.startsWith(`/${e}`) ? 'active' : '';
          return (
            <Accordion key={e} className="menu-item" square>
              <AccordionSummary className="label">
                <Link to={`/${e}`}>
                  <Button className={className} onClick={() => setIsDrawer(false)}>
                    {t(e)()}
                  </Button>
                </Link>
              </AccordionSummary>
            </Accordion>
          )
        })}
        <Accordion className="menu-item" square>
          <AccordionSummary expandIcon={<ExpandMore />} className="label">
            <Button>
              {t('language')()}
            </Button>
          </AccordionSummary>
          <AccordionDetails className="sub-menu">
            {LANGUAGES.map((e) => {
              const className = e === i18n.language.split('-')[0] ? 'active' : '';
              const onClick = () => {
                i18n.changeLanguage(e);
                setIsDrawer(false);
              }
              return (
                <Button key={e} className={className} onClick={onClick}>
                  {t(e)()}
                </Button>
              )
            })}
          </AccordionDetails>
        </Accordion>
      </Drawer>
    </AppBar>
  );
});

export default Header;