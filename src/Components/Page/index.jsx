import React from 'react';
import { Switch, Route, useLocation } from 'react-router-dom'; // https://reactrouter.com/web/api/Hooks/usehistory
import useTranslation from './../../hooks/useTranslation';
import Header from './Header';
import { Homepage, Projects, Contact } from './../Pages';
import { Container } from '@material-ui/core';

const PAGES = {
  projects: { Component: Projects, route: { path: '/projects' } },
  contact: { Component: Contact, route: { path: '/contact' } },
  homepage: { Component: Homepage, route: { path: '/*' } }
};

const Page = React.memo(() => {
  const { t } = useTranslation();
  const location = useLocation();
  
  let currentPage = location.pathname.replace(/\/$/, '').split('/').pop();
  if(currentPage && currentPage !== 'portfolio') {
    currentPage = `${t(currentPage)()} | `;
  }
  document.title = currentPage+t('appName')();

  const pages = [];
  for (const k in PAGES) {
    const { Component, route } = PAGES[k];
    pages.push(
      <Route key={k} {...route}>
        <Component />
      </Route>
    )
  }

  return (
    <Container maxWidth="lg">
      <Header />
      <Switch>
        {pages}
      </Switch>
    </Container>
  );
});

export default Page;