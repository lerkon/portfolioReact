import React from 'react';
import { Link } from 'react-router-dom';
import QueueAnim from 'rc-queue-anim';
import useTranslation from '../../hooks/useTranslation';
import { Grid, Typography, Button, makeStyles } from '@material-ui/core';
import { ArrowRightAlt } from '@material-ui/icons';

const useStyles = makeStyles(() => ({
  root: {
    marginTop : '120px'
  },
  intro: {
    maxWidth: '600px',
    '& a': {
      textDecoration: 'inherit',
      color: 'inherit',
      float: 'right',
      marginTop: '15px',
      '&:hover svg': {
        transform: 'translateX(5px)'
      },
      '& button': {
        borderRadius: '2rem',
        paddingRight: '20px',
        paddingLeft: '20px'
      },
      '& svg': {
        marginLeft: '5px',
        transition: 'transform 0.3s ease-in-out'
      }
    }
  },
}));

const Home = React.memo(() => {
  const styles = useStyles();
  const { t } = useTranslation();
  
  return (
    <Grid container direction="row" className={styles.root}>
      <Grid item className={styles.intro}>
        <QueueAnim type="left" delay={400}>
          <Typography key="1" component="h1" variant="h3" align="left" color="textPrimary" gutterBottom>
            {t('hi')()}
          </Typography>
          <Typography key="2" variant="h5" align="left" color="textSecondary" paragraph>
            {t('homepageIntroTxt')()}
          </Typography>
          <Link key="3" to="/projects">
            <Button color="primary" variant="contained" size="large">
              {t('viewMyWork')()} <ArrowRightAlt />
            </Button>
          </Link>
        </QueueAnim>
      </Grid>
    </Grid>
  );
});

export default Home;
