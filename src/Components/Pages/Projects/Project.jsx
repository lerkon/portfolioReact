import React, { useRef, useState } from 'react';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import images from './img';
import projects from './projects';
import useTranslation from '../../../hooks/useTranslation';
import {
  DialogActions, Typography, Button, makeStyles, Card, Dialog, DialogTitle, CardContent,
  CardActions, MobileStepper, IconButton, DialogContent, useMediaQuery, useTheme
} from '@material-ui/core';
import {
  WorkOutline, Close, KeyboardArrowRight, KeyboardArrowLeft, Code
} from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    '& img': {
      objectPosition: 'top',
      height: 255,
      maxWidth: 400,
      width: '100%',
      objectFit: 'cover'
    },
    '& .MuiMobileStepper-positionStatic': {
      paddingTop: '4px',
      paddingBottom: '4px',
      '& button': {
        padding: '0px'
      }
    },
    '& .card-btns': {
      float: 'right',
      paddingTop: '0px',
      '& .empty': {
        height: '30px'
      }
    },
    '& .empty-image-steps': {
      height: '32px',
      background: '#fafafa'
    }
  },
  modal: {
    '& h6': {
      display: 'flex',
      alignItems: 'center'
    },
    '& .close-btn': {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500]
    }
  },
  linkBtn: {
    textDecoration: 'inherit'
  }
}));

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const LinkBtn = React.memo(({ href, ...props }) => {
  const { t } = useTranslation();
  const styles = useStyles();
  
  return (
    <a href={href} className={styles.linkBtn} target="_blank" rel="noreferrer">
      <Button size="small" color="primary" {...props}>
        {t(`sourceCode`)()}
      </Button>
    </a>
  )
});

const ProjectModal = React.memo(({ id, visible, setVisible }) => {
  const { t } = useTranslation();
  const styles = useStyles();
  const theme = useTheme();
  const isFullScreen = !useMediaQuery(theme.breakpoints.up('sm'));

  const project = projects.list[id];
  const actionBtns = [];
  if (project.sourceCode) {
    actionBtns.push(<LinkBtn key="linkBtn" href={project.sourceCode} startIcon={<Code />} />)
  }

  return (
    <Dialog
      fullScreen={isFullScreen}
      open={visible}
      onClose={() => setVisible(false)}
      aria-labelledby="responsive-dialog-title"
      className={styles.modal}
    >
      <DialogTitle disableTypography>
        <Typography variant="h6">
          <WorkOutline />&nbsp;{t(`projectList.${id}.name`)()}
        </Typography>
        <IconButton aria-label="close" className="close-btn" onClick={() => setVisible(false)}>
          <Close />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
        {project.tools?.length > 0 && (
          <Typography gutterBottom>
            <b>{t(`tools`)()}:</b> {project.tools.join(', ')}
          </Typography>
        )}
        <Typography gutterBottom>
          <b>{t(`description`)()}:</b> {t(`projectList.${id}.description`)()}
        </Typography>
      </DialogContent>
      {actionBtns.length > 0 && (
        <DialogActions>
          {actionBtns}
        </DialogActions>
      )}
    </Dialog>
  )
});

const Project = React.memo(({ id }) => {
  const { t } = useTranslation();
  const [activeStep, setActiveStep] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const styles = useStyles();
  const timeoutRef = useRef({});
  
  const project = projects.list[id];
  const maxSteps = project.images.length;

  const set = (move) => {
    setActiveStep((current) => {
      if (move === '-') {
        return current - 1 < 0 ? maxSteps - 1 : current - 1;
      } else {
        return current + 2 > maxSteps ? 0 : current + 1;
      }
    });
  };

  const onMouseLeave = () => {
    clearTimeout(timeoutRef.current.timeout);
    clearInterval(timeoutRef.current.interval);
  }

  const onMouseEnter = () => {
    onMouseLeave();
    timeoutRef.current.timeout = setTimeout(set, 1000);
    timeoutRef.current.interval = setInterval(set, 4000);
  }

  const actionBtns = [];
  if (project.sourceCode) {
    actionBtns.push(<LinkBtn key="linkBtn" href={project.sourceCode} startIcon={<Code />} />)
  }
  if (t(`projectList.${id}.description`)()) {
    actionBtns.push(
      <Button key="learnMore" size="small" color="primary" onClick={() => setModalVisible(true)}>
        {t(`learnMore`)()}
      </Button>
    )
  }

  return (
    <>
      <Card className={styles.card} raised={false}>
        <AutoPlaySwipeableViews
          index={activeStep}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          style={{ cursor: project.gallery ? 'pointer' : 'default' }}
        >
          {project.images.map((e) => {
            let render;
            if (project.gallery) {
              render = (
                <a key={e} href={images[e]} target="_blank" rel="noreferrer">
                  <img src={images[e]} alt={id+1} />
                </a>
              )
            } else {
              render = <img key={e} src={images[e]} alt={id+1} />
            }
            return render;
          })}
        </AutoPlaySwipeableViews>
        {maxSteps > 1 ? (
          <MobileStepper
            steps={maxSteps}
            position="static"
            variant="dots"
            activeStep={activeStep}
            nextButton={
              <IconButton size="small" onClick={set}>
                <KeyboardArrowRight />
              </IconButton>
            }
            backButton={
              <IconButton size="small" onClick={() => set('-')}>
                <KeyboardArrowLeft />
              </IconButton>
            }
          />
        ) : (
          <div className="empty-image-steps" />
        )}
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {t(`projectList.${id}.name`)()}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {t(`projectList.${id}.shortTxt`)()}
          </Typography>
        </CardContent>
        <CardActions className="card-btns">
          {actionBtns.length ? actionBtns : <div className="empty" />}
        </CardActions>
      </Card>
      <ProjectModal id={id} visible={modalVisible} setVisible={setModalVisible} />
    </>
  )
});

export default Project;