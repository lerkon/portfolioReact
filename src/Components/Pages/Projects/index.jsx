import React from 'react';
import QueueAnim from 'rc-queue-anim';
import useTranslation from '../../../hooks/useTranslation';
import Container from './Container';
import { Grid, Typography, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    marginTop : '120px',
  },
  intro: {
    paddingTop: '0px !important'
  }
}));

const Projects = React.memo(() => {
  const { t } = useTranslation();
  const styles = useStyles();

  return (
    <Grid container className={styles.root} spacing={7}>
      <Grid item className={styles.intro} xs={12}>
        <QueueAnim type="left" delay={400}>
          <Typography key="1" component="h1" variant="h3" align="left" color="textPrimary" gutterBottom>
            {t('whatIdo')()}
          </Typography>
          <Typography key="2" variant="h5" align="left" color="textSecondary" paragraph>
            {t('projectIntroTxt')()}
          </Typography>
        </QueueAnim>
      </Grid>
      <Grid item xs={12}>
        <QueueAnim type="left" delay={400+300}>
          <Container key="1" />
        </QueueAnim>
      </Grid>
    </Grid>
  );
});

export default Projects;