import historical_data_1 from './historical_data.png';
import historical_data_2 from './historical_data_2.png';
import historical_data_3 from './historical_data_3.png';
import esp8266 from './ESP8266-EVB.webp';
import particle_photon from './particle_photon.webp';
import microservices from './microservices.svg';
import dataForwarder from './data_forwarder.png';
import portfolio from './portfolio.png';
import dashboard_1 from './dashboard.png';
import dashboard_2 from './dashboard_2.png';

const images = {
  historical_data_1, historical_data_2, historical_data_3, esp8266, particle_photon,
  microservices, dataForwarder, portfolio, dashboard_1, dashboard_2
};

export default images;


