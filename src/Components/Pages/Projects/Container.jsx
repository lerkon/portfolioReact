import React, { useState } from 'react';
import QueueAnim from 'rc-queue-anim';
import useTranslation from '../../../hooks/useTranslation';
import Project from './Project';
import projects from './projects';
import { Grid, Chip, useMediaQuery, useTheme } from '@material-ui/core';

const Container = React.memo(() => {
  const { t } = useTranslation();
  const [category, setCategory] = useState('all');
  const theme = useTheme();
  const isLarge = useMediaQuery(theme.breakpoints.up('lg'));

  let renderProjects = [];
  for (const k in projects.list) {
    const e = projects.list[k];
    if (e.category.includes(category) || category === 'all') {
      const i = renderProjects.length;
      renderProjects.push((
        <Grid key={category+i} item xs={12} sm={12} lg={4} style={{ maxWidth: '419px' }}>
          <QueueAnim type="bottom" delay={400+100*i}>
            <Project key={i} id={k} />
          </QueueAnim>
        </Grid>
      ));
    }
  }
  
  return (
    <Grid container spacing={3} justify="center">
      <Grid item xs={isLarge ? 12 : false}>
        <Grid container spacing={1}>
          {projects.categories.map((e) => (
            <Grid item key={e}>
              <Chip
                variant={category === e ? 'default' : 'outlined'}
                label={t(e)()}
                onClick={() => setCategory(e)}
                style={{ border: 'none' }}
              />
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={3} justify={isLarge ? "flex-start" : "center"}>
          {renderProjects}
        </Grid>
      </Grid>
    </Grid>
  )
});

export default Container;