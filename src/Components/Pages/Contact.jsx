import React from 'react';
import QueueAnim from 'rc-queue-anim';
import useTranslation from '../../hooks/useTranslation';
import { Form, FormItem } from './../Form';
import { Grid, Typography, makeStyles } from '@material-ui/core';
import { MailOutline } from '@material-ui/icons';

const useStyles = makeStyles(() => ({
  root: {
    marginTop : '120px'
  },
  submitBtn: {
    float: 'right'
  }
}));

const Contact = React.memo(() => {
  const styles = useStyles();
  const { t } = useTranslation();

  return (
    <>
      <Grid container justify="center" className={styles.root}>
        <Grid item xs={12} sm={9} md={12}>
          <QueueAnim type="left" delay={400}>
            <Typography key="1" component="h1" variant="h3" color="textPrimary" gutterBottom>
              {t('getInTouch')()}
            </Typography>
          </QueueAnim>
        </Grid>
      </Grid>
      <Grid container spacing={3} justify="center">
        <Grid item xs={12} sm={9} md={6}>
          <QueueAnim type="left" delay={400+100}>
            <Typography key="1" variant="h5" align="left" color="textSecondary" paragraph>
              {t('contactIntroTxt')()}
            </Typography>
          </QueueAnim>
        </Grid>
        <Grid item xs={12} sm={9} md={6}>
          <QueueAnim type="right" delay={400+200}>
            <Form key="1" action="https://formspree.io/f/mbjpvdzy" raw>
              <FormItem
                grid={{ xs: 12, sm: 6 }}
                variant="outlined"
                label={t('name')()}
                name="name"
                autoComplete="off"
                required
              />
              <FormItem
                grid={{ xs: 12, sm: 6 }}
                variant="outlined"
                label={t('email')()}
                name="_replyto"
                autoComplete="off"
                validator="email"
                required
              />
              <FormItem
                grid={{ xs: 12 }}
                variant="outlined"
                label={t('message')()}
                name="message"
                autoComplete="off"
                rows={3}
                required
                multiline
              />
              <FormItem
                grid={{ xs: 12 }}
                component="button"
                type="submit"
                variant="contained"
                color="primary"
                fullWidth={false}
                className={styles.submitBtn}
                startIcon={<MailOutline />}
              >
                {t('send')()}
              </FormItem>
            </Form>
          </QueueAnim>
        </Grid>
      </Grid>
    </>
  );
});

export default Contact;