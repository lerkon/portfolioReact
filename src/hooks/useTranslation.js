import { useCallback } from 'react';
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import i18nextBackend from 'i18next-xhr-backend';
import { useTranslation as useI18next, initReactI18next } from 'react-i18next';

if (process.env.NODE_ENV === 'development') {
  var missingTranslations = {};
  var timeout;
}

i18n
  .use(LanguageDetector)
  .use(i18nextBackend)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    load: 'languageOnly',
    detection: {
      caches: ['cookie']
    },
    backend: {
      loadPath: './locales/{{lng}}/{{ns}}.json'
    },
    debug: false,
    ns: ['translation'],
    supportedLngs: ['en', 'pl'],
    fallbackLng: 'en',
    saveMissing: true,
    interpolation: {
      escapeValue: false, // not needed for react!!
    },
    missingKeyHandler: (lng, ns, key, defaultVal, lngs) => {
      if (process.env.NODE_ENV === 'development') {
        missingTranslations[key] = key;
        clearTimeout(timeout);
        timeout = setTimeout(() => {
          const json = JSON.stringify(missingTranslations, null, 2);
          json.length > 2 && console.log('MISSING TRANSLATIONS:', json.slice(0, -1));
        }, 1000);
      }
    }
  });

const useTranslation = () => {
  const { t, ...rest } = useI18next();

  const get = useCallback((...args) => {
    const translated = t(...args);

    const transform = (capitalize) => {
      if (translated.constructor !== String) {
        return translated;
      }
      let str;
      switch (capitalize) {
        case 'each':
          str = translated?.replace(/(^\w{1})|(\s+\w{1})/g, (c) => c.toUpperCase());
          break;
        case 'all':
          str = translated?.toUpperCase();
          break;
        case 'no':
          str = translated;
          break;
        default:
          str = translated?.replace(/(^\w{1})/g, (c) => c.toUpperCase());
          break;
      }
      return str;
    }

    return transform;
  }, [t]);

  return { ...rest, t: get };
}

export default useTranslation;